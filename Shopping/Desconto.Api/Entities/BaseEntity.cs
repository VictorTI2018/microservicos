﻿namespace Desconto.Api.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }

        private DateTime? _createdAt;

        public DateTime? CreatedAt {
            get { return _createdAt; }
            set { _createdAt = (value is null ? DateTime.UtcNow : value); }
        }
    }
}
