﻿namespace Desconto.Api.Entities
{
    public class Cupom: BaseEntity
    {
        public string? ProductName { get; set; }

        public string? Description { get; set; }

        public int Amount { get; set; }
    }
}
