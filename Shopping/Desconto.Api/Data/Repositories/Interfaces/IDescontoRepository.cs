﻿using Desconto.Api.Entities;

namespace Desconto.Api.Data.Repositories.Interfaces
{
    public interface IDescontoRepository
    {
        Task<Cupom> GetAsync(string productName);

        Task<bool> CreateAsync(Cupom cupom);
        Task<bool> UpdateAsync(Cupom cupom);
        Task<bool> DeleteAsync(string productName);
    }
}
