﻿using Dapper;
using Desconto.Api.Data.Repositories.Interfaces;
using Desconto.Api.Entities;
using Npgsql;

namespace Desconto.Api.Data.Repositories
{
    public class DescontoRepository : IDescontoRepository
    {
        private readonly IConfiguration _configuration;
        public DescontoRepository(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        private NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(_configuration.GetSection("DatabaseSettings:ConnectionString").Value);
        }

        public async Task<Cupom> GetAsync(string productName)
        {
            NpgsqlConnection connection = GetConnection();

            var cupom = await connection.QueryFirstOrDefaultAsync<Cupom>(
                   "SELECT * FROM Cupom WHERE ProductName = @ProductName",
                   new { ProductName = productName }
              );
            return cupom;
        }

        public async Task<bool> CreateAsync(Cupom cupom)
        {
            NpgsqlConnection connection = GetConnection();
            var rowsAffected = await connection.ExecuteAsync(
                "INSERT INTO Cupom(ProductName, Description, Amount)" +
                "VALUES(@ProductName, @Description, @Amount)",
                new { ProductName = cupom.ProductName, Description = cupom.Description, Amount = cupom.Amount }
              );
            if (rowsAffected == 0) return false;

            return true;
        }


        public async Task<bool> UpdateAsync(Cupom cupom)
        {
            NpgsqlConnection connection = GetConnection();
            var rowsAffected = await connection.ExecuteAsync(
              "UPDATE Cupom SET ProductName = @ProductName, Description = @Description, Amount = @Amount",
              new { ProductName = cupom.ProductName, Description = cupom.Description, Amount = cupom.Amount }
             );
            if (rowsAffected == 0) return false;

            return true;
        }

        public async Task<bool> DeleteAsync(string productName)
        {
            NpgsqlConnection connection = GetConnection();
            var rowsAffected = await connection.ExecuteAsync(
                "DELETE FROM Cupom WHERE ProductName = @ProductName",
                new { ProductName = productName }
             );
            if (rowsAffected == 0) return false;

            return true;
        }
    }
}
