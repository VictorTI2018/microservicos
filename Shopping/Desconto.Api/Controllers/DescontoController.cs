﻿using Desconto.Api.Data.Repositories.Interfaces;
using Desconto.Api.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Desconto.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DescontoController : ControllerBase
    {
        private readonly IDescontoRepository _descontoRepository;

        public DescontoController(IDescontoRepository descontoRepository)
        {
            _descontoRepository = descontoRepository ?? throw new ArgumentNullException(nameof(descontoRepository));
        }

        [HttpGet("{productName}", Name = "GetDesconto")]
        [ProducesResponseType(typeof(Cupom), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Cupom>> Get(string productName)
        {
            if (productName is null) return BadRequest("Informe o nome do produto!");

            var cupom = await _descontoRepository.GetAsync(productName);

            return Ok(cupom);
        }

        [HttpPost]
        [ProducesResponseType(typeof(Cupom), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Cupom>> Create([FromBody] Cupom cupom)
        {
            if (cupom is null) return BadRequest("Dados inválidos");
            await _descontoRepository.CreateAsync(cupom);

            return CreatedAtRoute("GetDesconto", new { productName = cupom.ProductName }, cupom);

        }


        [HttpPut]
        [ProducesResponseType(typeof(Cupom), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Cupom>> Edit([FromBody] Cupom cupom)
        {
            if (cupom is null) return BadRequest("Dados inválidos");
            await _descontoRepository.UpdateAsync(cupom);

            return CreatedAtRoute("GetDesconto", new { productName = cupom.ProductName }, cupom);
        }

        [HttpDelete("{productName}", Name = "DeleteDesconto")]
        [ProducesResponseType(typeof(Cupom), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Cupom>> Delete(string productName)
        {
            if (productName is null) return BadRequest("Informe o nome do produto!");
            return Ok(await _descontoRepository.DeleteAsync(productName));
        }
    }
}
