﻿using Catalogo.Api.Data.Interfaces;
using Catalogo.Api.Entities;
using MongoDB.Driver;
using SharpCompress.Common;

namespace Catalogo.Api.Data
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IMongoCollection<TEntity> _entity;

        public BaseRepository(IConfiguration configuration)
        {
            var client = new MongoClient(configuration.GetSection("DatabaseSettings:ConnectionString").Value);
            var database = client.GetDatabase(configuration.GetSection("DatabaseSettings:DatabaseName").Value);

            _entity = database.GetCollection<TEntity>(typeof(TEntity).Name.ToLower());
        }


        public async Task<IEnumerable<TEntity>> GetAsync() => await _entity.Find(_ => true).ToListAsync();

        public async Task<TEntity> GetAsync(string id) => await _entity.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            try
            {
                await _entity.InsertOneAsync(entity);
                return entity;
            }catch(Exception ex)
            {
                throw new Exception($"Error ao cadastrar - {ex.Message}");
            }
        }

        public async Task<bool> UpdateAsync(TEntity entity)
        {
            try
            {
                FilterDefinition<TEntity> filter = Builders<TEntity>.Filter.Eq(x => x.Id, entity.Id);
                var updateResult = await _entity.ReplaceOneAsync(filter, replacement: entity);

                return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error ao cadastrar - {ex.Message}");
            }
        }

        public async Task<bool> DeleteAsync(string id)
        {
            try
            {
                FilterDefinition<TEntity> filter = Builders<TEntity>.Filter.Eq(x => x.Id, id);
                DeleteResult deleteResult = await _entity.DeleteOneAsync(filter);

                return deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error ao cadastrar - {ex.Message}");
            }
        }
    }
}
