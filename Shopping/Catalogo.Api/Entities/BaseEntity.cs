﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Catalogo.Api.Entities
{
    public abstract class BaseEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        private DateTime? _createdAt;

        public DateTime? Created_At
        {
            get { return _createdAt; }
            set { _createdAt = (value is null ? DateTime.UtcNow : value); }
        }
    }
}
