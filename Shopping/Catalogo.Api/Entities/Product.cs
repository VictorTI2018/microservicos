﻿using Catalogo.Api.Validation;
using MongoDB.Bson.Serialization.Attributes;

namespace Catalogo.Api.Entities
{
    public class Product: BaseEntity
    {

        [BsonElement("Nome")]
        public string Name { get; set; }
        [BsonElement("Description")]
        public string Description { get; set; }
        [BsonElement("CategoryName")]
        public string CategoryName { get; set; }

        [BsonElement("Price")]
        public decimal Price { get; set; }


    }
}
