﻿

namespace Catalogo.Api.Dto
{
    public class ProductDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }

        public decimal Price { get; set; }

    }
}
