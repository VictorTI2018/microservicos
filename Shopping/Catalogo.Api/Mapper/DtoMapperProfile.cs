﻿using AutoMapper;
using Catalogo.Api.Dto;
using Catalogo.Api.Entities;

namespace Catalogo.Api.Mapper
{
    public class DtoMapperProfile: Profile
    {
        public DtoMapperProfile()
        {
            CreateMap<ProductDto, Product>();
            CreateMap<Product, ProductDto>();
        }
    }
}
