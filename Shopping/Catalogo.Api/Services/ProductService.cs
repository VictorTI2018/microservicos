﻿using AutoMapper;
using Catalogo.Api.Data.Interfaces;
using Catalogo.Api.Dto;
using Catalogo.Api.Entities;
using Catalogo.Api.Services.Interfaces;

namespace Catalogo.Api.Services
{
    public class ProductService : IProductService
    {
        private readonly IBaseRepository<Product> _baseRepository;
        private readonly IMapper _mapper;
        public ProductService(IBaseRepository<Product> baseRepository, IMapper mapper)
        {
            _baseRepository = baseRepository;
            _mapper = mapper;
        }
        public async Task<Product> CreateAsync(ProductDto entity)
        {
            return await _baseRepository.CreateAsync(_mapper.Map<Product>(entity));
        }



        public async Task<IEnumerable<Product>> GetAsync() => await _baseRepository.GetAsync();

        public async Task<Product> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id)) throw new ArgumentException("Por favor, informe o Id do produto!");

            return await _baseRepository.GetAsync(id);
        }

        public async Task<bool> UpdateAsync(ProductDto entity)
        {
            return await _baseRepository.UpdateAsync(_mapper.Map<Product>(entity));
        }

        public async Task<bool> DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id)) throw new ArgumentException("Por favor, informe o Id do produto!");
            return await _baseRepository.DeleteAsync(id);
        }
    }
}
