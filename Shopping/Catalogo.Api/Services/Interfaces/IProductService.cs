﻿using Catalogo.Api.Dto;
using Catalogo.Api.Entities;

namespace Catalogo.Api.Services.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAsync();

        Task<Product> GetAsync(string id);

        Task<Product> CreateAsync(ProductDto entity);

        Task<bool> UpdateAsync(ProductDto entity);

        Task<bool> DeleteAsync(string id);
    }
}
