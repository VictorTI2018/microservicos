using AutoMapper;
using Catalogo.Api.Data;
using Catalogo.Api.Data.Interfaces;
using Catalogo.Api.Mapper;
using Catalogo.Api.Services;
using Catalogo.Api.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region DI Repository
builder.Services.AddSingleton(typeof(IBaseRepository<>), typeof(BaseRepository<>));
builder.Services.AddSingleton<IProductService, ProductService>();
#endregion

#region Mapper
var configMapper = new MapperConfiguration(cfg =>
{
    cfg.AddProfile(new DtoMapperProfile());
});

IMapper mapper = configMapper.CreateMapper();
builder.Services.AddSingleton(mapper);

#endregion

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
