﻿namespace Catalogo.Api.Validation
{
    public class DomainValidation: ArgumentException
    {
        public DomainValidation(string errorMessage) : base(errorMessage) { }

        public static void Validate(bool hasError, string errorMessage)
        {
            if (hasError) throw new DomainValidation(errorMessage);
        }
    }
}
