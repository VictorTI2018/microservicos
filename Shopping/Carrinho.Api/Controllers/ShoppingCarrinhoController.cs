﻿using Carrinho.Api.Data.Repositories.Interfaces;
using Carrinho.Api.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Carrinho.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ShoppingCarrinhoController : ControllerBase
    {
        private readonly ICarrinhoRepository _carrinhoRepository;
        public ShoppingCarrinhoController(ICarrinhoRepository carrinhoRepository)
        {
            _carrinhoRepository = carrinhoRepository ?? throw new ArgumentNullException(nameof(carrinhoRepository));
        }

        [HttpGet("{userName}", Name = "GetCarrinho")]
        [ProducesResponseType(typeof(ShoppingCarrinho), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ShoppingCarrinho>> Get(string userName)
        {
            if (userName is null) return BadRequest("Informe um userName!");

            var carrinho = await _carrinhoRepository.GetAsync(userName);

            return Ok(carrinho ?? new ShoppingCarrinho(userName));
        }

        [HttpPost]
        [ProducesResponseType(typeof(ShoppingCarrinho), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<ShoppingCarrinho>> Edit([FromBody] ShoppingCarrinho shoppingCarrinho)
        {
            if (shoppingCarrinho is null) return BadRequest("Dados inválido");

            return Ok(await _carrinhoRepository.UpdateAsync(shoppingCarrinho));
        }

        [HttpDelete("{userName}", Name = "DeleteCarrinho")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Delete(string userName)
        {
            if (userName is null) return BadRequest("Informe um userName!");
            await _carrinhoRepository.DeleteAsync(userName);
            return Ok();
        }
    }
}
