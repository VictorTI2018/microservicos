﻿namespace Carrinho.Api.Entities
{
    public class ShoppingCarrinho
    {
        public string UserName { get; set; }

        public List<CarrinhoItem> Items { get; set; } = new List<CarrinhoItem>();

        public ShoppingCarrinho() { }

        public ShoppingCarrinho(string userName)
        {
            UserName = userName;
        }

        public decimal TotalItems
        {
            get
            {
                decimal totalPrice = 0;
                foreach(var item in Items)
                {
                    totalPrice += item.Price * item.Quantity;
                }
                return totalPrice;
            }
        }
    }
}
