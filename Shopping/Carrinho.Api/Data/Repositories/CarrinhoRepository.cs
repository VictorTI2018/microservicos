﻿using Carrinho.Api.Data.Repositories.Interfaces;
using Carrinho.Api.Entities;
using Microsoft.Extensions.Caching.Distributed;
using System.Text.Json;

namespace Carrinho.Api.Data.Repositories
{
    public class CarrinhoRepository : ICarrinhoRepository
    {
        private readonly IDistributedCache _redisCache;

        public CarrinhoRepository(IDistributedCache redisCache)
        {
            _redisCache = redisCache ?? throw new ArgumentNullException(nameof(redisCache));
        }


        public async Task<ShoppingCarrinho> GetAsync(string userName)
        {
            var carrinho = await _redisCache.GetStringAsync(userName);

            if (string.IsNullOrEmpty(carrinho)) return null;

            return JsonSerializer.Deserialize<ShoppingCarrinho>(carrinho);
        }

        public async Task<ShoppingCarrinho> UpdateAsync(ShoppingCarrinho shoppingCarrinho)
        {
            await _redisCache.SetStringAsync(shoppingCarrinho.UserName, JsonSerializer.Serialize(shoppingCarrinho));

            return await GetAsync(shoppingCarrinho.UserName);
        }

        public async Task DeleteAsync(string userName)
        {
            await _redisCache.RemoveAsync(userName);
        }
    }
}
