﻿using Carrinho.Api.Entities;

namespace Carrinho.Api.Data.Repositories.Interfaces
{
    public interface ICarrinhoRepository
    {
        Task<ShoppingCarrinho> GetAsync(string userName);

        Task<ShoppingCarrinho> UpdateAsync(ShoppingCarrinho shoppingCarrinho);

        Task DeleteAsync(string userName);
    }
}
